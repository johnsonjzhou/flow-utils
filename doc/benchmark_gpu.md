# Benchmark GPU  

Does a quick stress test for GPU based transitions and triggers a custom event 
that provides the average frames per second (fps).  

## Triggers
`benchmark-gpu-result` on `window`  

## Syntax

````javascript
import { BenchmarkGPU } from 'flow-utils';

// listen for the result
window.addEventListener('benchmark-gpu-result', (event) => {
  const { detail: fps } = event;
  // do something
});

// trigger a stress test
(new BenchmarkGPU()).measure();
````

---
## License
MIT.  
Johnson Zhou [johnson@simplyuseful.io](mailto://johnson@simplyuseful.io).  