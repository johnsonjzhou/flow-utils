# Color Scheme  

A collection of methods to handle light and dark color schemes. 

---
## useColorScheme

Checks `localStorage` for any stored user preference, if not, use the system 
setting.  

**Stored Preference**  
Preferences are stored in the `localStorage` with the key 
`prefers-color-scheme` and values of `light || dark`.  

**System Setting**  
System settings are checked against the `prefers-color-scheme` media query 
using the `matchMedia` method.  

### Syntax  

````javascript
import { useColorScheme } from 'flow-utils';

const colorScheme = useColorScheme();
console.log(colorScheme);
// light or dark
````

#### Parameter(s)  
None.  

### Return value(s)  
`string` with value of *light* or *dark*  

--- 
## setColorScheme

Sets a value to the `prefers-color-scheme` item of the `localStorage` 
then invokes a callback if supplied.  

### Syntax  

````javascript
import { setColorScheme } from 'flow-utils';

setColorScheme('dark', (scheme) => {
  // do something

  console.log(scheme);
  // dark
});
````

### Parameter(s)  

***scheme*** `string`  
The color scheme in either *auto*, *light* or *dark*.  

***callback*** `function`  
A callback to call when the scheme has been set, which takes a single 
argument that is the value of the scheme. The callback will not be called 
if the browser does not support `localStorage`.  

### Return value(s) 
Void.  

---
## watchSystemScheme  

Watches system color scheme change event by attaching a handler and 
calling the supplied callback with the scheme color. The watch handler is 
for the `prefers-color-scheme` media query using the `matchMedia` method. 
The watch process will not take place if there is already a user preference 
set in `localStorage`.  

### Syntax  

````javascript
import { watchSystemScheme } from 'flow-utils';

watchSystemScheme((scheme) => {
  // do something

  console.log(scheme);
  // light or dark
});
````

### Parameter(s)  

***callback*** `function`  
A callback to call when the scheme has been set, which takes a single 
argument that is the value of the scheme. The callback will not be called 
if the browser does not support `MediaQueryListEvent`.  

### Return value(s)  
Void.  

---
## getPreferredColorScheme

Checks and returns the value of 'prefers-color-scheme' in localStorage.  

### Syntax  

````javascript 
import { getPreferredColorScheme } from 'flow-utils';

getPreferredColorScheme();
// null or string
````

### Parameter(s)  
None.  

### Return value(s)  
`null` or a `string` representing the preferred value.  

--- 
## Examples  

````javascript
import { useColorScheme, setColorScheme, watchSystemScheme } from 'flow-utils';

const applyColorScheme = (scheme) => {
  switch (scheme) {
    case 'light':
      // apply light styling
    break;
    case 'dark':
      // apply dark styling
    break;
  }
};

// on load, eg during componentDidMount
// apply the current scheme then watch for changes 
applyColorScheme(useColorScheme());
watchSystenScheme(applyColorScheme);

// set the color scheme on button press
<Button onclick={ () => setColorScheme('dark) } > 
````

---
## License
MIT.  
Johnson Zhou [johnson@simplyuseful.io](mailto://johnson@simplyuseful.io).  