# Console

A wrapper for the built in `Console` object, that provides styled 
output as well as `Bugsnag` notifications in error handling.  

![Console screenshot](../img/console_screenshot.jpg)  

> **Webpack required**  
> To use Bugsnag, ensure that the Bugsnag key and app version are loaded 
> with the constants `___WEBPACK_BUGSNAG___` and `___WEBPACK_VERSION___` 
> via `webpack.DefinePlugin`.  

---
## Syntax

````javascript
import { Console } from 'flow-utils';

// initiate constructor in global scope and initialise Bugsnag
window.Console = new Console(scope, color);
window.Console.registerBugsnag();

// initiate constructor in class scope
this.Console = new Console(scope, color);

// standard logging
window.Console.log(message, context);

// error logging
window.Console.notify(message, context);

// catching errors
const errorHandler = (error) => {
  // do something
};

this.Console.registerErrorHandler(errorHandler); 

try {
  // erroneous
} catch (error) {
  this.Console.handleError(error);
}
````

---
## Parameters 

***scope*** `string`  
Name of the main badge. Usually this means the scope of the output.  

***color*** `string`  
Color code to use for the badge output.  

---
## Methods of the Console constructor  
- [Console.registerBugsnag](#console.registerbugsnag)  
- [Console.log](#console.log)  
- [Console.notify](#console.notify)  
- [Console.registerErrorHandler](#console.registererrorhandler)  
- [Console.handleError](#console.handleError)  

---
## Console.registerBugsnag  

Registers `Bugsnag` as a global instance in `window.bugsnagClient`.  

### Parameters

***`___WEBPACK_BUGSNAG___`*** `string`  
The Bugsnag API key, loaded as a constant via `webpack.DefinePlugin`.

***`___WEBPACK_VERSION___`*** `string`  
The app version, for version tracking, loaded as a constant via 
`webpack.DefinePlugin`.  

### Return value(s)  
Void.  

---
## Console.log  

A drop in replacement for the standard `console.log`. This will create a 
styled output in the the console when in development mode, or leave a 
Bugsnag breadcrumb in production.  

### Parameters

***message*** `mixed`  
The output required.  

***context*** `string`  
The name of the secondary badge, useful to identify what the output is for.  

### Return value(s)  
Void.  

### Fires
`window.bugsnagClient.breadcrumb`  

---
## Console.notify  

Equivalent to `Console.log`, except this is used specifically for errors. 
Styles the output in red in development mode and triggers a Bugsnag 
notification in production.  

### Parameters 

***error*** `error`  
An error object.  

***context*** `string`  
The name of the secondary badge, useful to identify where the error occurred.  

### Return value(s)  
Void.  

### Fires  
`window.bugsnagClient.notify`  

---
## Console.registerErrorHandler  

Registers a predefined error handler callback in the class, 
that will be invoked by `Console.handleError`.  

### Parameters  

***callback*** `function`  
The callback function will receive one argument, being the `Error` object.  

### Return value(s)  
Void.

---
## Console.handleError  

For handling `catch` events with ease, notifies the error, 
then calls a predefined callback.  

### Parameters  

***error*** `Error`  
The caught `Error` event.  

### Return value(s)
Void.  

### Fires  
`Console.notify`  
`Console.errorHandler` as set by `Console.registerErrorHandler`  

---
## License
MIT.  
Johnson Zhou [johnson@simplyuseful.io](mailto://johnson@simplyuseful.io).  