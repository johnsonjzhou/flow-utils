# Flow Web Worker utilities

## Usage

````javascript
import { CustomWorker, registerWorker } from 'flow-utils';
````

---
## CustomWorker  

`object`  
An extensible worker template, constructed as an object 
This has a built in `toString` function so that when registered, 
all the properties can be merged as a string in a Blob.  

### Properties  

***CustomWorker.toString***  
A getter function that invokes the `toString` method of all declared properties 
that are functions. The function string is then prefixed with `self.` 
so that it can be called as such from within the web worker thread.  

> **Limitation**  
> Declared properties that are not functions are not supported.  

### Return value(s)
An `object`.  

---
## registerWorker

`function`  
Registers a web worker when using with the workerTemplate, 
returns the web worker instance.  

### Parameters

***worker***  
An object constructed with `CustomWorker`.  

### Return value(s)  

A `Worker` object or `null`.  

---
## Syntax

**Web Worker**  

````javascript
// create the web worker code
const custom_worker = new CustomWorker();

// handle messages from main thread
custom_worker.onmessage = function(message) {
  // do something
}

// assign web worker methods
custom_worker.method = function() {
  // send message to main thread
  self.postMessage();
};

// reference another method in self
custom_worker.method2 = function() {
  self.method();
}

````

**Application**  

````javascript
// register and run the web worker
const webWorker = registerWorker(custom_worker);

// send message to web worker thread
webWorker.postMessage(message);

// handle messages from web worker thread
webWorker.onmessage = (message) => {
  // do something
};

````

---
## License
MIT.  
Johnson Zhou [johnson@simplyuseful.io](mailto://johnson@simplyuseful.io).  