# Changelog

## 1.0.4
 - Added DateTime  

## 1.0.5
 - Changed `dependencies` to `devDependencies`  
 - Added message to validationPatterns.money

## 1.0.4 
 - Added in Color Scheme: getPreferredColorScheme

## 1.0.3 
 - Data helper: nonMatchingFields  

## 1.0.2 
 - React helpers: traverseChildBranch  

## 1.0.1 
- Text field validation rules   

## 1.0.0
- Console  
- Misc UI Helpers  
- Misc Data Helpers  
- Color Scheme  
- BenchmarkGPU including PerformanceFPS  
- Perspective3D  

## 0.0.1  
- Web Worker  