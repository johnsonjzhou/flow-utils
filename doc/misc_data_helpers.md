# Misc Data Helpers

## Methods  

### unixTimestamp  
Returns a unix time stamp in milliseconds.  
````javascript
const timestamp = unixTimestamp();
// 1600656061017
````

### randKey  
Generates a random key.  
````javascript
const key = randKey();
// OGguPR

const key = randKey(12);
// cGvKLgmMUX0R
````

### pluralise  
Returns an 's' whether a count is singular or multiple.  
````javascript
const colors = ['red', 'orange', 'green'];
console.log(`The color${pluralise(colors.length)} are amazing!`);
// The colors are amazing!
````

### clone
Returns a deep clone of an object using `Object.create`.  
````javascript
const object2 = clone(object1);
// Object {...}
````

### checkArraysSame  
Compare whether two arrays are essentially the same on the basis of 
type, number of items and each item is the same.  
> **Credit:**  [Go Make Things](https://gomakethings.com/check-if-two-arrays-or-objects-are-equal-with-javascript/)  
````javascript
const array1 = [...];
const array2 = [...];

console.log(checkArraySame(array1, array2));
// true || false
````

### isEmptyObject  
Check whether an object is empty  
> **Credit:**  [Coderwall](https://coderwall.com/p/_g3x9q/how-to-check-if-javascript-object-is-empty)  
````javascript
const object1 = {...};

console.log(isEmptyObject(object1));
// true || false
````

### objectHasKeys  
Checks whether certain keys are present in an object.  
````javascript
const object1 = {...};
const keys = ['one', 'two', 'three'];
console.log(objectHasKeys(object1, keys));
// true || false
````

### findInArray  
Searches an array for a particular term and returns true or false.  
> **Deprecated**  
> Use `Array.prototype.find`, may need to be polyfilled
````javascript
const array1 = [...];
console.log(findInArray('value', array1));
// true || false
````

### parseInputType  
Checks array of input types and return the first relevant type.  
````javascript
const type = 'input email';
const types = type.split(' ');
// ['input', 'email']
console.log(parseInputType(types));
// email
````

### getNameValue  
For use with an array of object containin 'name' and 'value' properties, 
searches for the specified name, then return the associated value, 
if there are multiple matching objects, return value from the first object.  
````javascript
const data = [
  { name: 'City', value: 'Melbourne' },
  { name: 'State', value: 'Victoria' }
];
console.log(getNameValue('City', data));
// Melbourne
````

### validationPatterns  
An `object` containing common validation patterns in RegExp for text fields.  
````javascript
import { validationPatterns } from 'flow-utils';

const rule = validationPatterns.email;
// { pattern, message }

rule.pattern.test('hello@email.com');
// true

console.log(rule.pattern.constructor.name);
// RegExp
````

### nonMatchingFields 
For use in determining whether two named fields have matching values. 
eg. Whether a "Confirm Password" field matches a "Password" field. 
````javascript
import { nonMatchingFields } from 'flow-utils';

const data_matching = [
  { name: 'password', value: '1234' }, 
  { name: 'password_confirm', value: '1234' }
];

const data_different = [
  { name: 'password', value: '1234' }, 
  { name: 'password_confirm', value: '12' }
];

nonMatchingFields(data_matching, 'password', 'password_confirm');
// false 

nonMatchingFields(data_different, 'password', 'password_confirm', 
  'Does not match Password'
);
// 'Does not match Password' 
````

---
## License
MIT.  
Johnson Zhou [johnson@simplyuseful.io](mailto://johnson@simplyuseful.io).  