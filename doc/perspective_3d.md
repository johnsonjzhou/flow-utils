# Perspective3D

Handles 3D perspective based on mouse cursor for desktop or device orientation. 
Designed to be used on the `perspective-origin` css property, so that changes 
in mouse position or device orientation will cause changes to the perspective. 
This uses a `Web Worker` to perform calculations.  

**Limitations**  
- Works in Chromium based desktop and mobile browsers.  
- Works in desktop Firefox, but Firefox updates the DOM very slowly when 
applying CSS changes inline, so works but with lag.  
- Does not work properly in mobile Firefox, as `deviceorientation` works 
differently.  

--- 
## Events  

This uses `CustomEvent` for interactions on a global scale.  

Event | Target | Process
-- | -- | -- 
`perspective3d-change` | `window` | Fires 
`perspective3d-watch` | `window` | Listens 
`perspective3d-disconnect` | `window` | Listens 

---
## Syntax and examples 

````javascript
import { Perspective3D } from 'flow-utils'; 

// initiate constructor
const perspective = new Perspective3D();

// listen for perspective changes
window.addEventListener('perspective3d-change', (event) => {
  const { detail } = event;
  // apply the perspective-origin to an element 
  // this needs to be the parent of elements with z axis transformations 
  // this method does not work well in Firefox, very very slow...
  element.setProperty('perspective-origin', detail);

  console.log(detail);
  // x% y%
  // eg. 40% 50%
});

// start watching for changes
window.dispatchEvent(new CustomEvent('perspective3d-watch'));

// stop watching for changes 
window.dispatchEvent(new CustomEvent('perspective3d-disconnect'));

// release the constructor, like a manual garbage collection 
// eg when used in componentWillUnmount 
perspective.release();

````


---
## License
MIT.  
Johnson Zhou [johnson@simplyuseful.io](mailto://johnson@simplyuseful.io).  