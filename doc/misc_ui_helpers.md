# Miscellaneous UI helpers

## Methods

### rgba  
Use like Sass rgba()  
````javascript
rgba(hex, 0.1)
// rgba(x, x, x, 0.1)
````

### lighten  
Use like Sass lighten()  
````javascript
lighten(hex, 0.1)
// #HEX
````

### darken  
Use like Sass darken()  
````javascript
darken(hex, 0.1)
// #HEX
````

### blend  
Blends two colors together based on a specific amount  
````javascript
blend(hex1, hex2, 0.5)
// #HEX
// a value of 1 means entirely hex2
````

### msieVersion  
Check if user is using Internet Explorer.  
````javascript
msieVersion();
// 11 (if IE)
// false (if not IE)
````

### beforeUnload
Shows a prompt before window unloads eg: 
*"there are unsaved changes on this page, do you want to reload?"*  
````javascript
import { beforeUnload } from 'flow-utils';
window.addEventListener('beforeunload', beforeUnload);
````

### traverseChildBranch  
Designed to be used with `React.Children.map`,  
Applies a handler function to React child elements that are at the tip 
of the children branch (elements that have no further nesting) 
@param  {React.Child}  child - from React.Children.map 
@param  {function}  handler - handler function to apply to the child 

handler will be invoked with the child as the only argument 
````javascript  
import React, { cloneElement } from 'react'; 
import { traverseChildBranch } from 'flow-utils'; 

const Component = (props) => {
  
  const {
    children 
  } = props; 
  
  ... 

  handler = (child) => {
    const newProps = {
      ...
    };

    return cloneElement(child, newProps);
    // React will warn if key not included in newProps 
  };

  return (
    <>{ 
      children.map(child => traverseChildBranch(child, handler))
    } </>
  )
};
````  

---
## License
MIT.  
Johnson Zhou [johnson@simplyuseful.io](mailto://johnson@simplyuseful.io).  