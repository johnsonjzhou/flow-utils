/**
 * Common validation patterns for input text fields
 * @author Johnson Zhou (johnson@simplepharmacy.io)
 * 
 * @export  validationPatterns  
*/

const validationPatterns = {
  email: {
    pattern: /^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/,
    message: 'Irregular email format',
  }, 
  password: {
    pattern: /^\S*$/i,
    message: 'Avoid using spaces',
  }, 
  tel: {
    pattern: /^\({0,1}((0|\+61|\+61\s)(2|4|3|7|8)){1}\){0,1}(\ |-){0,1}[0-9]{2}(\ |-){0,1}[0-9]{2}(\ |-){0,1}[0-9]{1}(\ |-){0,1}[0-9]{3}$/,
    message: 'Landline or Mobile number only',
  }, 
  letters: {
    pattern: /^[a-z\s]+$/i, 
    message: 'Letters only', 
  },
  number: {
    pattern: /^[0-9.]{0,}$/,
    message: 'Numbers only',
  }, 
  ccVisa: {
    pattern: /^(?:4[0-9]{12}(?:[0-9]{3})?)$/,
  }, 
  ccMaster: {
    pattern: /^(?:5[1-5][0-9]{14})$/,
  }, 
  ccAmex: {
    pattern: /^(?:3[47][0-9]{13})$/,
  }, 
  ccCVC: {
    pattern: /^[0-9]{3,4}$/,
  }, 
  ccMM: {
    pattern: /^((0[1-9])|(1[0-2]))/,
  }, 
  ccYY: {
    pattern: /^[0-9]{2}$/,
  }, 
  ccYYYY: {
    pattern: /^[0-9]{4}$/,
  }, 
  money: {
    pattern: /^\d+(\.\d{1,2})?$/,
    message: 'Numbers only to two decimals'
  }, 
};

export {
  validationPatterns
};