/**
 * Data handling helper functions - misc tools
 * @author Johnson Zhou <johnson@simplyuseful.io>
 * 
 * @export  unixTimestamp
 * @export  randKey
 * @export  pluralise
 * @export  clone
 * @export  checkArraysSame
 * @export  isEmptyObject
 * @export  objectHasKeys
 * @export  findInArray
 * @export  parseInputType
 * @export  getNameValue
*/

/**
 * Returns a unix time stamp in milliseconds
 */
const unixTimestamp = () => {
  return Math.round((new Date()).getTime());
}

/**
 * Generates a random key
 * @param  {integer}  max - character length of the key returned
 * @return  {string}
 */
const randKey = (max = 6) => {
  let key = '';
  var chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
  for (var i = 0; i < max; i++)
    key += chars.charAt(Math.floor(Math.random() * chars.length));
  return key;
}

/**
 * Returns an 's' whether a count is singular or multiple
 * @param {int} n 
 */
const pluralise = (n = 1) => {
  return (n > 1) && 's' || '';
}

/**
 * Returns a new object using Object.create
 */
const clone = (obj) => {
  return Object.create(obj);
}

/**
 * Compare whether two arrays are essentially the same on the basis of
 * type, number of items, each item is the same
 * @author  Go Make Things
 * @reference  https://gomakethings.com/check-if-two-arrays-or-objects-are-equal-with-javascript/
 */
const checkArraysSame = (value, other) => {
  // Get the value type
	var type = Object.prototype.toString.call(value);

	// If the two objects are not the same type, return false
	if (type !== Object.prototype.toString.call(other)) return false;

	// If items are not an object or array, return false
	if (['[object Array]', '[object Object]'].indexOf(type) < 0) return false;

	// Compare the length of the length of the two items
	var valueLen = type === '[object Array]' ? value.length : Object.keys(value).length;
	var otherLen = type === '[object Array]' ? other.length : Object.keys(other).length;
	if (valueLen !== otherLen) return false;

	// Compare two items
	var compare = function (item1, item2) {

		// Get the object type
		var itemType = Object.prototype.toString.call(item1);

		// If an object or array, compare recursively
		if (['[object Array]', '[object Object]'].indexOf(itemType) >= 0) {
			if (!isEqual(item1, item2)) return false;
		}

		// Otherwise, do a simple comparison
		else {

			// If the two items are not the same type, return false
			if (itemType !== Object.prototype.toString.call(item2)) return false;

			// Else if it's a function, convert to a string and compare
			// Otherwise, just compare
			if (itemType === '[object Function]') {
				if (item1.toString() !== item2.toString()) return false;
			} else {
				if (item1 !== item2) return false;
			}

		}
	};

	// Compare properties
	if (type === '[object Array]') {
		for (var i = 0; i < valueLen; i++) {
			if (compare(value[i], other[i]) === false) return false;
		}
	} else {
		for (var key in value) {
			if (value.hasOwnProperty(key)) {
				if (compare(value[key], other[key]) === false) return false;
			}
		}
	}

	// If nothing failed, return true
	return true;
}

/**
 * Check whether an object is empty
 * @reference  https://coderwall.com/p/_g3x9q/how-to-check-if-javascript-object-is-empty
 * @param {object} object 
 */

const isEmptyObject = (object) => {
  return (Object.keys(object).length < 1);
}

/**
 * Checks whether certain keys are present in an object
 * @param  {object}  object 
 * @param  {array}  keys 
 *
 * @return  {boolean} 
 */
const objectHasKeys = (object, keys) => {
  // check that object is an object
  if (typeof object !== 'object') return false;
  
  // grab all the keys from object
  const objectKeys = Object.keys(object);
  
  // check and see if keys are present in testKeys
  let totalKeys = keys.length;
  let foundKeys = keys.filter(key => {
    return objectKeys.indexOf(key) >= 0;
  }).length;

  return (foundKeys === totalKeys);
}

/**
 * Searches an array for a particular term and returns true or false
 * @param {string} find 
 * @param {array} array 
 * @return  {boolean}
 */
const findInArray = (find, array) => {
  return (array.indexOf(find) >= 0);
}

/**
 * Checks array of input types and return the first relevant type
 * @param {array} types - eg: ['input', 'email']
 * @return  {boolean}  false - if array does not contain 'input'
 * @return  {string} - returns the first available valid type
 */
const parseInputType = (types) => {

  if (types.indexOf('input') < 0) return false;

  const filterTerms = ['input', 'textarea', 'disabled'];

  return types.filter(type => {
    return (filterTerms.indexOf(type) < 0);
  })[0];
}

/**
 * For use with an array of object containin 'name' and 'value' properties,
 * searches for the specified name, then return the associated value,
 * if there are multiple matching objects, return value from the first object
 * @param  {string}  name
 * @param  {array}  data - array of objects, []{name, value}
 * @return  {mixed|null} - returns null if error
 */
const getNameValue = (name, data) => {
  try {
    return data.
    filter(object => object.name === name)
    [0]
    .value;
  } catch (error) {
    return null;
  }
}

/**
 * For use in determining whether two named fields have matching values. 
 * eg. Whether a "Confirm Password" field matches a "Password" field. 
 * @param  {array}  data - of objects { name, valid, value }
 * @param  {string}  field_1 - the master field to compare to 
 * @param  {string}  field_2 - the slave field 
 * @param  {string}  [message] - custom rejection message 
 * 
 * @return  {false|string}  - false = fields match, otherwise reject with a message
 */
const nonMatchingFields = (
  data = [], 
  field_1, 
  field_2, 
  message = 'Does not match'
) => {
  const master = data.find(field => field.name === field_1);
  const slave = data.find(field => field.name === field_2);

  if (!master || !slave) return false;

  return (slave.value && slave.value !== master.value) && message 
    || 
  false;
}

export {
  unixTimestamp, 
  randKey, 
  pluralise, 
  clone, 
  checkArraysSame, 
  isEmptyObject, 
  objectHasKeys, 
  findInArray, 
  parseInputType, 
  getNameValue, 
  nonMatchingFields, 
};