/**
 * An extension of the Date class that provides additional features including: 
 * - DateTime.parse will recognise a variety of commonly used formats (british)
 * - DateTime.toString will return in a user friendly (british) format 
 * 
 * @author  Johnson Zhou  <johnson@simplyuseful.io>  
 * 
 * @export  DateTime as default  
 * 
 * todo  getCalendar()  
 */

/**
 * @method  Date.parse()  
 * @method  toString()  
 * @method  toJSON()  
 * @method  getVerboseMonth()  
 * @method  getVerboseDay()  
 */
class DateTime extends Date {

  /**
   * @param  {string|int}  input  either a unix timestamp (milliseconds) or
   *  a string containing the date that is able to be parsed by DateTime.parse()
   */
  constructor(input) {
    switch (typeof input) {
      case 'number':
        // if number, assume unix time stamp 
        return super(input);

      case 'string':
      default:
        // parse a string input 
        const {
          year, month, date, hour, minutes, seconds 
        } = DateTime.parse(input);
    
        return super(year, month-1, date, hour, minutes, seconds);
    }
  }

  /**
   * Replaces Date.parse() that can take a variety of date and time 
   * representations that are commonly used by humans. Note, date and month 
   * positions are mostly based on the British format.  
   * @param  {string}  string  the input string  
   * 
   * @return  {object}  { year, month, date, hour, minutes, seconds }
   * 
   * @example  dates  
   * 2020-03-01 || 2020/03/01  
   * 01-03-2020 || 01/03/2020  
   * 1-3-20 || 1-3 || 1 Mar || 1 March  
   * 
   * @example  times  
   * 14:30:23 || 14:30 || 2:30pm || 2pm || 2 pm  
   */
  static parse = (string = '') => {

    /**
     * @see  https://javascript.info/regexp-groups#capturing-groups-in-replacement
     */

    const now = new Date();

    const datetime = {
      year: now.getFullYear(), 
      month: now.getMonth() + 1, // returns 0-11, need 1-12
      date: now.getDate(), 
      hour: now.getHours(), 
      minutes: now.getMinutes(), 
      seconds: now.getSeconds(), 
    };

    let parsedDate = {} , parsedTime = {};

    const matchAndParseJson = (string, pattern, output) => {
      const matched = string.replace(pattern, output);
      return JSON.parse(matched.slice(matched.indexOf('{'), matched.indexOf('}')+1));
    };

    // handle date portion 

    const datePatterns = [
      // 01032020 
      /(?:^|[\s])(?<date>[0-9]{2})(?<month>[0-9]{2})(?<year>[0-9]{4})(?:$|[\s])/,

      // 010320 
      /(?:^|[\s])(?<date>[0-9]{2})(?<month>[0-9]{2})(?<year>[0-9]{2})(?:$|[\s])/,

      // 2020-03-01 || 2020/03/01 || 2020 03 01
      /(?:^|[\s])(?<year>[0-9]{4})(?:[-/\s]{1})(?<month>[0-9]{1,2})(?:[-/\s]{1})(?<date>[0-9]{1,2})(?:$|[\s])/, 

      // 01-03-2020 || 01/03/2020 || 1-3-20 || 1-3 || 1 Mar || 1 March
      /(?:^|[\s])(?<date>[0-9]{1,2})(?:[-/\s]{1})(?<month>[0-9]{1,2}|[A-Za-z]{3,})(?:[-/\s]{1})?(?<year>[0-9]{2,4})?(?:$|[\s])/, 

      // Mar 1 || March 1 || Mar 1 20 || Mar 1 2020
      /(?:^|[\s])(?<month>[0-9]{1,2}|[A-Za-z]{3,})(?<date>^[0-9]{1,2})(?:[-/\s]{1})?(?<year>[0-9]{2,4})?(?:$|[\s])/,
    ];
  
    for (const pattern of datePatterns) {
      if (pattern.test(string)) {
        const output = '{"year":"$<year>","month":"$<month>","date":"$<date>"}';
        parsedDate = matchAndParseJson(string, pattern, output);

        // check and manage parsed date object 
        Object.keys(parsedDate).forEach((property) => {
          switch (property) {
            case 'year':
              // year is a two digit number, add the first two digits from current
              parsedDate[property].length === 2 && Number(parsedDate[property] !== 'NaN') && 
              (parsedDate[property] = `${String(datetime.year).substr(0,2)}${parsedDate[property]}`);
              break;

            case 'month': 
              switch (isNaN(parsedDate[property])) {
                // month is a named string rather than number  
                case true: 
                  const names = [ 'jan', 'feb', 'mar', 'apr', 'may', 'jun', 
                    'jul', 'aug', 'sep', 'oct', 'nov', 'dec'
                  ];

                  parsedDate[property] = names.findIndex((month) => {
                    return String(parsedDate[property])
                    .toLowerCase()
                    .startsWith(month);
                  }) + 1;
                  // 0, 1-12
                  // 0 will be removed later
                  break;

                // month is a number, check for invalid entry 
                case false:
                  const number = Number(parsedDate[property]);

                  ((number < 1) || (number > 12)) && 
                  (parsedDate[property] = 0);
                  // this will be removed later 
                  break;
              }
              break;

            case 'date':
              switch (isNaN(parsedDate[property])) {
                // date is a named string rather than number  
                case true: 
                  parsedDate[property] = 0;
                  break;

                // date is a number, check for invalid entry 
                case false:
                  const number = Number(parsedDate[property]);

                  ((number < 1) || (number > 31)) && 
                  (parsedDate[property] = 0);
                  // this will be removed later 
                  break;
              }
              break;
          }

          // remove empty or invalid properties 
          if (!parsedDate[property]) {
            delete parsedDate[property];
            return;
          }
          
          // cast to number to number
          parsedDate[property] = Number(parsedDate[property]);
        });

        break;
      }
    }

    // handle time portion 

    const timePatterns = [

      // 2pm || 2 pm 
      /(?:^|[\s])(?<hour>[0-9]{1,2})(?:[\s]{1})?(?<meridiem>am|pm)(?:$|[\s])/i,

      // 14:30 || 2:30PM 
      /(?:^|[\s])(?<hour>[0-9]{1,2})(?:[.:]{1})(?<minutes>[0-9]{1,2})(?:[\s]{1})?(?<meridiem>am|pm)?(?:$|[\s])/i, 

      // 14:30:23 ||  || 2:30:23PM
      /(?:^|[\s])(?<hour>[0-9]{1,2})(?:[.:]{1})(?<minutes>[0-9]{1,2})(?:[.:]{1})?(?<seconds>[0-9]{1,2})?(?:[\s]{1})?(?<meridiem>am|pm)?(?:$|[\s])/i, 
    ];

    for (const pattern of timePatterns) {
      if (pattern.test(string)) {
        const output = '{"hour":"$<hour>","minutes":"$<minutes>","seconds":"$<seconds>","meridiem":"$<meridiem>"}';
        parsedTime = matchAndParseJson(string, pattern, output);

        // check and manage parsed time object 
        Object.keys(parsedTime).forEach((property) => {
          switch (property) {
            case 'hour': 
              switch (isNaN(parsedTime[property])) {
                // hour is a named string rather than number  
                case true: 
                  parsedDate[property] = 0;
                  break;

                // hours is a number, check for invalid entry 
                case false:
                  const number = Number(parsedTime[property]);

                  if ((number < 0) || (number > 23)) {
                    (parsedTime[property] = 0);
                    // this will be removed later
                    break; 
                  } 

                  if (parsedTime.hasOwnProperty('meridiem')) {
                    switch(parsedTime.meridiem.toLowerCase()) {
                      case 'am':
                        (number > 12) && (parsedTime[property] = (number - 12));
                        break;

                      case 'pm':
                        (number < 12) && (parsedTime[property] = (number + 12));
                        break;
                    };
                  }

                  break;
              }
              break;

            case 'minutes':
            case 'seconds':
              switch (isNaN(parsedTime[property])) {
                // minutes or seconds is a named string rather than number  
                case true: 
                  parsedTime[property] = 0;
                  break;

                // minutes or seconds is a number, check for invalid entry 
                case false:
                  const number = Number(parsedTime[property]);

                  ((number < 0) || (number > 59)) && 
                  (parsedTime[property] = 0);
                  // this will be removed later 
                  break;
              }
              break;
          }
          
          // cast to number to number
          (property !== 'meridiem') && (parsedTime[property] = Number(parsedTime[property]));
        });

        break;
      }
    }

    return {...datetime, ...parsedDate, ...parsedTime};
  };

  /**
   * Replaces Date.toString() method, 
   * returns a more friendly date time string 
   * that can also be parsed by DateTime.parse()  
   * 
   * @return  {string}  2 Mar 2021 2:48PM 
   */
  toString = () => {
    const date= this.getDate();
    const month = this.getVerboseMonth();
    const year = this.getFullYear();
    let hour = this.getHours();
    const minutes = this.getMinutes();
    const meridiem = (hour > 12) && 'PM' || 'AM';

    (hour > 12) && (hour = hour - 12);

    const pad = int => String(int).padStart(2, '0');

    return `${date} ${month} ${year} ${hour}:${pad(minutes)}${meridiem}`;
  }

  /**
   * Replaces Date.toJSON() method  
   * @return  {string}
   */
  toJSON = () => this.toString();

  /**
   * Returns the English text of the month 
   * @param  {bool=false}  [long]  full text or shortened to 3 characters  
   * @param  {int}  [lookup]  look up a custom month or leave blank to use current  
   * 
   * @return  {string}
   */
  getVerboseMonth = (long = false, lookup = null) => {
    const names = [
      'January', 
      'February', 
      'March', 
      'April', 
      'May', 
      'June', 
      'July', 
      'August', 
      'September', 
      'October', 
      'November', 
      'December', 
    ];

    const month = 
      (lookup !== null) && (lookup <= 12) && names[lookup-1] || 
      names[this.getMonth()];

    return long && month || month.slice(0, 3);
  }

  /**
   * Returns the English text of the day of the week 
   * @param  {bool=false}  [long]  full text or shortened to 3 characters 
   * @param  {int}  [lookup]  look up a custom day or leave blank to use current 
   * 
   * @return  {string}
   */
  getVerboseDay = (long = false, lookup = null) => {
    const names = [
      'Sunday',
      'Monday', 
      'Tuesday', 
      'Wednesday', 
      'Thursday', 
      'Friday', 
      'Saturday',
    ];

    const day = 
      (lookup !== null) && (lookup <= 6) && names[lookup] || 
      names[this.getDay()];

    return long && day || day.slice(0, 3);
  }

  /**
   * Returns an object with information that can be displayed as a 
   * calendar date picker 
   * @param  {int=0}  [offset]  advance or rewind by a certain number of months  
   * @param  {int=1}  [weekstart]  day of the week to start, default is Monday  
   * 
   * @return  {object}  { month, calendar(array) }
   * @return  {object}  Object.calendar[]{ date, time, current_month }
   * 
   * todo  work in progress
   */
  getCalendar = (offset = 0, weekstart = 1) => {

    const cursor = new DateTime(this.getTime());

    // handle month offset
    (offset !== 0) && cursor.modify(offset, 'month');

    const month = cursor.getMonth();
    const date = cursor.getDate();
    const day = cursor.getDay() || 7;             // 0 to 6, assign 0 as 7
    const monthVerbose = cursor.getVerboseMonth(true); 

    // rewind the cursor to the start of the week 
    cursor.modify((day - weekstart), 'day');

    // rewind the cursor to the earliest day that will allow for a 
    // grid of calendar dates to be populated
    do {
      cursor.modify(-7, 'week');
    } while (cursor.getMonth >= month);

    let calendar = [];

    cursor.modify(-rewind, 'day');
    
    // generate 5 weeks of calendar entries 
    for (let i = 0; i < 35; i++) {

      calendar.push({
        date: cursor.getDate(), 
        time: cursor.getTime(), 
        current_month: (cursor.getMonth() === month)
      });

      cursor.modify(1, 'day');
    }

    return { month: monthVerbose, calendar };
  }

  /**
   * Modify the DateTime by the offset amount for a given unit 
   * @param  {int=0}  [offset]
   * @param  {string}  unit - minute, hour, day, month, year
   */
  modify = (offset = 0, unit) => {

    let modifier = 0;
    const minutes = 1000 * 60;
    const hours = minutes * 60;
    const days = hours * 24;
    const years = days * 365;

    switch(unit) {
      case 'minute':
        modifier = offset * minutes;
      break;

      case 'hour':
        modifier = offset * hours;
      break;

      case 'day':
        modifier = offset * days;
      break;

      case 'year':
        modifier = offset * years;
      break;

      case 'month':
        const currentMonth = this.getMonth();
        let newMonth = currentMonth + offset;
        let newYear = this.getFullYear();
        
        do {
          newYear--;
          newMonth = newMonth + 12;
        } while (newMonth < 0);

        do {
          newYear++;
          newMonth = newMonth - 12;
        } while (newMonth > 11);

        this.setMonth(newMonth);
        this.setFullYear(newYear);

      break;
    }

    this.setTime(this.getTime() + Math.round(modifier));
  }
}

export {
  DateTime as default
};