/**
 * Handler for styled console output as well as various debugging functions,
 * including subscribing to Bugsnag
 * @author  Johnson Zhou <johnson@simplyuseful.io>
 * 
 * @export  Console as default
 * 
 * Bugsnag apiKey and appVersion are defined by Webpack constants
 * @require  webpack.DefinePlugin
 * @constant  {string}  ___WEBPACK_BUGSNAG___
 * @constant  {string}  ___WEBPACK_VERSION___
 */
import bugsnag from 'bugsnag-js';
import { blend } from '../ui/misc_ui_helpers'; 
import { badgeStyling } from './console_styles';

class Console {
  
  /**
   * @param  {string}  scope - name of the name badge indicating process
   * @param  {string}  color - color of the styling
   */
  constructor(scope = 'Console', color = 'black') {

    // bind methods
    this.registerBugsnag = this.registerBugsnag.bind(this);
    this.registerErrorHandler = this.registerErrorHandler.bind(this);

    // for styled console output
    this.scope = scope;
    this.colors = {
      scopeFg: 'white',
      scopeBg: color,
      contextFg: color,
      contextBg: blend(color, 'whitesmoke', 0.95)
    }

    // whether we are in production mode
    this.productionMode = (process.env.NODE_ENV === 'production');

    // callback function for error handler
    this.errorHandler = null;

    // Webpack constants for Bugsnag
    this.bugsnagKey = ___WEBPACK_BUGSNAG___ || null;
    this.bugsnagVer = ___WEBPACK_VERSION___ || null;
  }

  /**
   * Setter, 
   * Registers Bugsnag as a global instance
   */
  registerBugsnag() {
    if (!(this.bugsnagKey && this.bugsnagVer))
    return this.log('Bugsnag key and version required via Webpack', 'Bugsnag');
    
    window.bugsnagClient = bugsnag({
      apiKey: this.bugsnagKey,
      appVersion: this.bugsnagVer,
      consoleBreadcrumbsEnabled: false,
      releaseStage: process.env.NODE_ENV,
      autoDetectErrors: this.productionMode,
    });
    this.log('Global Bugsnag initialised', 'Bugsnag');
  }

  /**
	 * Outputs debugging messages to JS console
   * or leave a breadcrumb in Bugsnag in production
	 * @param	 {string}	 message
   * @param  {string}  context
   * 
   * @fires  window.bugsnagClient.leaveBreadcrumb
	 */
  log(message, context = null, colors = this.colors) {
    if (this.productionMode) {
      typeof window !== 'undefined' && 
      window.bugsnagClient && bugsnagClient.leaveBreadcrumb(message);
    } else {
      // parse message type and apply correct placeholders
      let placeholder;
      switch (typeof message) {
        case 'string':
        case 'number':
        case 'boolean':
        case 'undefined':
          placeholder = '%s';
        break;
        default:
          placeholder = '%o';
        break;
      }

      // produce the console.log output based on whether a context is supplied
      // context is a secondary badge with inverted and lighter colors
      if (context) {
        console && console.log(
          `%c${this.scope}%c %c${context}%c\n`+
          `${placeholder}`,
          badgeStyling(colors.scopeBg, colors.scopeFg), null,
          badgeStyling(colors.contextBg, colors.contextFg), null,
          message
        );
      } else {
        console && console.log(
          `%c${this.scope}%c\n`+
          `${placeholder}`,
          badgeStyling(colors.scopeBg), null,
          message
        );
      }
    }
  }
  
  /**
   * Setter, 
   * leaves Bugsnag breadcrumb only in production
   * 
   * @fires  window.bugsnagClient.leaveBreadcrumb
   */
  breadcrumb(message) {
    (this.productionMode) &&
    (typeof window !== 'undefined') && 
    window.bugsnagClient &&
    bugsnagClient.leaveBreadcrumb(message);
  }

  /**
   * Setter, 
	 * Outputs debugging messages to JS console
   * or notifies Bugsnag in production
	 * @param	 {string}	 error
   * 
   * @fires  window.bugsnagClient.notify
	 */
  notify(error, context) {
    // development mode, write to JS console
    if (!this.productionMode) 
    return this.log(
      error,
      context || error.name,
      {
        scopeFg: 'white',
        scopeBg: 'maroon',
        contextFg: 'maroon',
        contextBg: 'lightpink'
      }
    );

    // production mode, notify bugsnag
    (typeof window !== 'undefined' && 
    window.bugsnagClient && window.bugsnagClient.notify(error))
  }

  /**
   * Setter, 
   * Sets the Bugsnag user info
   * @param	 {object}  object 
   * @param  {string}  object.id
   * @param  {string}  object.name
   * @param  {string}  object.email
   * @param  {string}  object.role
   */
  user({ id, name, email, role }) {
    typeof window !== 'undefined' && 
    window.bugsnagClient && 
    (window.bugsnagClient.user = { id, name, email, role });
  }

  /**
   * Setter, 
   * Sets the Bugsnag screen name, the last viewed screen
   * @param  {object}  object
   * @param  {string}  object.screen
   */
  screen({ screen }) {
    typeof window !== 'undefined' && 
    window.bugsnagClient &&
    (window.bugsnagClient.request = { screen });
  }

  /**
   * Setter, 
   * Registers a predefined error handler callback,
   * that will be invoked by this.handleError 
   */
  registerErrorHandler(method) {
    typeof method === 'function' && (this.errorHandler = method);
  }

  /**
   * For handling catch events with ease, 
   * notifies the error, then calls a predefined callback
   * @param  {Error}  error
   */
  handleError(error) {
    this.notify(error);
    this.errorHandler && this.errorHandler(error);
  }
}

export {
  Console as default 
};