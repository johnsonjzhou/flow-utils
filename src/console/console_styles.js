/**
 * Styling for console output
 * @author  Johnson Zhou <johnson@simplyuseful.io>
 * 
 * @export  badgeStyling
 */

const badgeStyling = (
  background = 'black', 
  foreground = 'white' 
) => `
  display: inline-block;
  text-transform: uppercase;
  border-radius: 0.4em;
  color: ${foreground};
  background: ${background};
  font-size: 0.8em;
  padding: 0.2em 0.4em;
`;

export {
  badgeStyling 
};
