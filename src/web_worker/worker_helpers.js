/**
 * Helpers to manage Web Workers
 * @author  Johnson Zhou <johnson@simplyuseful.io>
 * 
 * @export  CustomWorker 
 * @export  registerWorker 
 */

/**
 * An extensible worker template, constructed as an object
 * This has a built in toString function so that when registered, 
 * all the properties can be merged as a string in a Blob
 */
const workerTemplate = {
  toString() {
    let workerString = '';
    for (name in this) {
      name !== 'toString' && 
      typeof this[name] === 'function' && 
      (workerString += `self.${name} = ` + this[name].toString() + ';');
    }
    return workerString;
  }
};

/**
 * Returns an instance of workerTemplate
 * @return  {object}
 */
const CustomWorker = function() {
  return Object.create(workerTemplate);
};

/**
 * Registers a web worker when using with the workerTemplate,
 * returns the web worker instance.
 * @param  {object}  worker - ideally based on the workerTemplate
 * @return  {Worker|null}
 */
const registerWorker = (worker = workerTemplate) => {
  return ('Worker' in window && new Worker(URL.createObjectURL(
    new Blob([ worker.toString() ], {type : 'application/json'})
  ))) 
  || null;
};

export {
  CustomWorker, 
  registerWorker 
};