/**
 * Handlers for light or dark color scheme
 * @author  Johnson Zhou <johnson@simplyuseful.io>
 * 
 * @export  useColorScheme
 * @export  setColorScheme
 * @export  watchSystemScheme
 * @export  getPreferredColorScheme
 */

/**
 * Checks the system color scheme by accessing the
 * prefers-color-scheme media query
 * @param  {function}  [onchange] - handler for the onchange event
 * @return  {string='light'}  light | dark
 */
const checkSystemColorScheme = (onchange) => {
  const systemIsDark = window.matchMedia('(prefers-color-scheme: dark)');
  onchange && (systemIsDark.onchange = onchange);
  return (systemIsDark && systemIsDark.matches && 'dark') || 'light';
}

/**
 * Checks and returns the value of 'prefers-color-scheme' in localStorage
 * @return  {null|string} - light | dark
 */
const checkUserPreference = () => {
  return window.localStorage 
  && window.localStorage.getItem('prefers-color-scheme');
}

/**
 * Checks localStorage for any stored preferences,
 * if no stored preferences are found, check the system setting
 * @returns  {string}  light | dark
 */
const useColorScheme = () => {
  return checkUserPreference() || checkSystemColorScheme();
}

/**
 * Sets a value to the 'prefers-color-scheme' item of the localStorage
 * then invokes a callback if supplied
 * @param  {string}  scheme - light | dark | auto
 * @param  {function}  [callback]
 * 
 * @note  light | dark - sets value then calls callback with value
 * @note  auto - removes value then calls callback with system value
 */
const setColorScheme = (scheme, callback) => {
  if (!window.localStorage) return;
  (new Promise((resolve) => {
    switch (scheme) {
      case 'light': 
      case 'dark': 
        window.localStorage.setItem('prefers-color-scheme', scheme);
        resolve(scheme);
      break;
      case 'auto':
        window.localStorage.removeItem('prefers-color-scheme');
        resolve(checkSystemColorScheme());
      break;
      default:
        resolve(null);
      break;
    }
  }))
  .then(scheme => scheme && callback && callback(scheme));
}

/**
 * Watches system color scheme change event by attaching a handler
 * and calling the supplied callback with the scheme color
 * @param  {function}  callback
 */
const watchSystemScheme = (callback) => {
  if (typeof callback !== 'function') return;

  /**
   * @param  {MediaQueryListEvent}  event
   */
  const onchange = (event) => {
    if (
      !(event.constructor && event.constructor.name === 'MediaQueryListEvent')
    ) return;

    // skip if there is a user preference already set
    if (checkUserPreference()) return;

    const { matches } = event;
    const scheme = matches && 'dark' || 'light';
    callback.call(null, scheme);
  }
  checkSystemColorScheme(onchange);
}

export {
  useColorScheme, 
  setColorScheme, 
  watchSystemScheme, 
  checkUserPreference as getPreferredColorScheme, 
};