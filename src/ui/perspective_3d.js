/**
 * Handles 3D perspective based on mouse cursor for desktop or
 * device orientation 
 * @author  Johnson Zhou  <johnson@simplyuseful.io>
 * 
 * @note  this version uses a web worker to perform calculations
 * 
 * @listens  window#perspective3d-watch
 * @listens  window#perspective3d-disconnect
 * @fires  window#perspective3d-change
 * 
 * @method  release -- garbage collection
 * 
 * @note  this.callback({x, y})
 * x and y are expressed in percent value from 0 to 100
 * 
 * @note  window.DeviceOrientationEvent requires HTTPS in chromium
 */
import Console from '../console/console';

/**
 * Web worker for perspective calculations
 */
let perspective_worker = {
  
  /**
   * Receives a message from the main thread and split the work
   * based on type
   * @param  {event}  event
   * @param  {object}  event.data
   * @param  {string}  event.data.type - mousemove || deviceorientation
   * @param  {object}  event.data.payload
   */
  onmessage: function({ data }) {
    const { type, payload } = data;
    switch (type) {
      case 'mousemove':
        return self.calculateMouseMove(payload);
      case 'deviceorientation':
        return self.calculateDeviceOrientation(payload);
    }
  },

  round: function(number) {
    return Math.ceil(number / 1) * 1;
  }, 

  /**
   * Calculates x and y coordinates for a mousemove event
   * based on supplied coordinates,
   * and postMessage the coordinates as { x, y } back to the main thread.
   * @param  {object}  payload
   * @param  {int}  payload.mouseX - mouse position
   * @param  {int}  payload.mouseY - mouse position
   * @param  {int}  payload.startX - starting position
   * @param  {int}  payload.startY - starting position
   * @param  {int}  payload.viewportX - viewport position
   * @param  {int}  payload.viewportY - viewport position
   * 
   * @return  {void}
   */
  calculateMouseMove: function({ 
    mouseX, 
    mouseY, 
    startX, 
    startY, 
    viewportX, 
    viewportY, 
  }) {

    // get the changed position relative to starting position
    const changed = {
      x: mouseX - startX,
      y: mouseY - startY
    }

    // axis change calculations
    const deltaX = (changed.x / viewportX * 20);
    const deltaY = (changed.y / viewportY * 20);

    // works off default perspective of (30, 40) for desktop based on css
    const x = self.round(30 + deltaX);
    const y = self.round(40 + deltaY);

    // apply callback only if axis changes have occurred
    (deltaX !== 0 && deltaY !== 0) && 
    self.postMessage({ x, y });
  },

  /**
   * Calculates x and y coordinates for a deviceorientation event
   * based on supplied coordinates,
   * and postMessage the coordinates as { x, y } back to the main thread.
   * @param  {object}  payload
   * @param  {int}  payload.alpha - event
   * @param  {int}  payload.beta - event
   * @param  {int}  payload.gamma - event
   * @param  {int}  payload.startAlpha - starting position
   * @param  {int}  payload.startBeta - starting position
   * @param  {int}  payload.startGamma - starting position
   * 
   * @return  {void}
   */
  calculateDeviceOrientation: function({
    alpha, 
    beta, 
    gamma, 
    startAlpha, 
    startBeta, 
    startGamma 
  }) {
    // event.alpha = rotate on horizontal plane (z axis = vertical axis)
    // 0 to 360, north = 0, ++ counter clock wise

    // event.beta = rotate forwards and backwards (x axis = horizontal axis)
    // -180 to 180, parallel to ground = 0, ++ top away from user

    // event gamma = rotate by twist left and right (y axis = toward/away axis)
    // -90 to 90, parallel to ground = 0, ++ twist right

    // get the changed in orientation relative to start
    const changed = {
      alpha: alpha - startAlpha, 
      beta: beta - startBeta, 
      gamma: gamma - startGamma 
    };

    // translating 3D coordinate changes to 2D is quite involved
    // so we are only going to consider the beta and gamma
    // alpha will be ignored (twisting on horizontal plane)
    // gamma changes will be x coordinate changes (range -45 to 45)
    // beta changes will be y coordinate changes (range -30 to 30)
    // range determined by experimentation
    // a value of 0 means mid point of perspective, aka 50%
    // match starting values to default applied to Mobile devices in css

    // axis change calculations
    const deltaX = (50 * (changed.gamma / 45));
    const deltaY = (50 * (changed.beta / 30));

    const x = 0 - deltaX;
    const y = 30 - deltaY;

    // apply callback only if axis changes have occurred
    (deltaX !== 0 && deltaY !== 0) && 
    self.postMessage({ x, y });
  },

  toString() {
    let workerString = '';
    for (name in this) {
      name !== 'toString' && 
      (workerString += `self.${name} = ` + this[name].toString() + ';');
    }
    return workerString;
  }
};

class Perspective3D {
  
  constructor() {

    // uncomment this for debug
    // this.Console = new Console('Perspective 3D', '#6633FF');

    this.supportedUA = true;  // works with most browsers

    // the starting orientation
    // once set this will be an object { alpha, beta, gamma }
    this.startingOrientation = null;

    // the starting position, case desktop
    // once set this will be an object { x, y }
    this.startingPosition = null;

    // internal value to track coordinates and avoid
    // dispatching events unnecessarily
    this.perspectiveOrigin = {
      x: 50,        // percent value
      y: 50,        // percent value
    }

    this.watching = false;      // whether we are currently watching

    // setters
    this.resetStartingOrientation = this.resetStartingOrientation.bind(this);
    this.resetStartingPosition = this.resetStartingPosition.bind(this);

    // handlers
    this.handleMouseMove = this.handleMouseMove.bind(this);
    this.handleDeviceOrientation = this.handleDeviceOrientation.bind(this);

    // listeners
    this.watch = this.watch.bind(this);
    this.disconnect = this.disconnect.bind(this);
    window.addEventListener('perspective3d-watch', this.watch);
    window.addEventListener('perspective3d-disconnect', this.disconnect);

    // triggers
    this.dispatchChangeEvent = this.dispatchChangeEvent.bind(this);

    // web worker
    this.worker = null;
  }

  /**
   * Binds both mousemove & deviceorientation event listeners to window
   * 
   * @note  doing it this way may potentially have a conflict,
   * whereby a device may have both mouse and orientation events
   * But..  it will be unlikely to use the mouse and tilt the device
   * at the same time
   * 
   * @note  Chromium will fire a deviceorientation event immediately
   * after adding the listener
   */
  watch() {
    if (this.watching === true) return;

    // this.Console.log('Watch');

    window.MouseEvent && 
    window.addEventListener('mousemove', this.handleMouseMove);

    window.DeviceOrientationEvent && 
    window.addEventListener(
      'deviceorientation', this.handleDeviceOrientation
    );

    this.worker = new Worker(URL.createObjectURL(
      new Blob([ perspective_worker.toString() ], {type : 'application/json'})
    ));
    this.worker.onmessage = this.dispatchChangeEvent;

    this.watching = true;
  }

  /**
   * Removes event listener on window
   */
  disconnect() {
    if (this.watching === false) return;

    // this.Console.log('Disconnected');

    window.removeEventListener('mousemove', this.handleMouseMove);
    window.removeEventListener(
      'deviceorientation', this.handleDeviceOrientation
    );
    this.resetStartingOrientation();
    
    this.worker && this.worker.terminate();

    this.watching = false;
  }

  resetStartingOrientation() {
    this.startingOrientation = null;
  }

  resetStartingPosition() {
    this.startingPosition = null;
  }

  /**
   * Receives the calculated coordinates back from the worker thread
   * and apply this to the callback
   * 
   * @listens  MessageEvent
   * @param  {event}  event
   * @param  {object}  event.data
   * @param  {int}  event.data.x - the x coordinate of perspective-origin
   * @param  {int}  event.data.y - the y coordinate of perspective-origin
   * 
   * @fires  window#perspective3d-change - detail: x% y%
   */
  dispatchChangeEvent({ data }) {
    const { x, y } = data;
    const { currentX, currentY } = this.perspectiveOrigin;
    // skip if coordinates haven't changed
    if (x === currentX && y === currentY) return;
    data && window.dispatchEvent(new CustomEvent(
      'perspective3d-change', { detail: `${x}% ${y}%`}
    ));
    this.perspectiveOrigin = { x, y };
  };

  /**
   * Handles mousemove event by
   * extracting mouse coordinates from the event and applying a set
   * of coordinates to the worker thread for calculation
   * @listens  MouseEvent
   * @param  {event}
   */
  handleMouseMove(event) {
    if (event.type !== 'mousemove') return;

    const { 
      screenX: mouseX, 
      screenY: mouseY, 
    } = event;

    const { 
      innerWidth: viewportX,
      innerHeight: viewportY
    } = window;

    // set the startingPosition if hasn't been set
    (this.startingPosition === null) && 
    (this.startingPosition = { startX: mouseX, startY: mouseY });

    const {
      startX, 
      startY, 
    } = this.startingPosition;

    this.worker && 
    this.worker.postMessage({ 
      type: 'mousemove', 
      payload: {
        mouseX, 
        mouseY, 
        startX, 
        startY, 
        viewportX, 
        viewportY 
      }
    });
  }

  /**
   * Handles deviceorientation event by
   * extracting orientation coordinates from the event and applying a set
   * of coordinates to the worker thread for calculation
   * @listens  DeviceOrientationEvent
   * @param  {event}
   */
  handleDeviceOrientation(event) {
    if (event.type !== 'deviceorientation') return;

    const {
      alpha, 
      beta, 
      gamma 
    } = event;

    // set the startingOrientation if hasn't been set
    (this.startingOrientation === null) && 
    (this.startingOrientation = {
      startAlpha: alpha, 
      startBeta: beta, 
      startGamma: gamma
    });

    const {
      startAlpha, 
      startBeta, 
      startGamma 
    } = this.startingOrientation;

    this.worker && 
    this.worker.postMessage({ 
      type: 'deviceorientation', 
      payload: {
        alpha, 
        beta, 
        gamma, 
        startAlpha, 
        startBeta, 
        startGamma, 
      }
    });
  }

  /**
   * Garbage collection
   */
  release() {
    window.removeEventListener('perspective3d-watch', this.watch);
    window.removeEventListener('perspective3d-disconnect', this.disconnect);
    window.removeEventListener('mousemove', this.handleMouseMove);
    window.removeEventListener(
      'deviceorientation', this.handleDeviceOrientation
    );
    return true;
  }
}

export { 
  Perspective3D as default
};