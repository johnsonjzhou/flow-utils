/**
 * Helper functions for when using the React library 
 * @author  Johnson Zhou <johnson@simplyuseful.io> 
 * 
 * @export  traverseChildBranch 
 */

import React, { cloneElement } from 'react';

/**
 * Designed to be used with React.Children.map
 * Applies a handler function to React child elements that are at the tip 
 * of the children branch (elements that have no further nesting) 
 * @param  {React.Child}  child - from React.Children.map 
 * @param  {function}  handler - handler function to apply to the child 
 * @param  {mixed}  key - the key to apply to the element list 
 * 
 * handler will be invoked with the child as the only argument 
 */
const traverseChildBranch = (child, handler, key) => { 

  const {
    props: { children } 
  } = child;

  // determine if nesting exists 
  // nesting exists when children of child is an array 
  const nested = Array.isArray(children);

  // return:
  // 1. if the children is nested, traverse the branch again, 
  // 2. if we are at the tip of the branch (no more nesting), return and invoke 
  //    the handler function, this should return a version of the child, 
  // 3. if no handler function is specified, return the original child 
  return nested && 
    cloneElement(child, { 
      key, 
      children: children.map(
        (nChild, nKey) => traverseChildBranch(nChild, handler, nKey)
      )
    })
      || 
    (handler && handler(child)) 
      || 
    child
  ;
};

export { 
  traverseChildBranch 
};