/**
 * A tool to measure frames per second
 * @author  Johnson Zhou <johnson@simplyuseful.io>
 */

class PerformanceFPS {
  
  constructor() {
    this.requestId = undefined;
    this.frames = 0;
    this.startTime = null;

    this.start = this.start.bind(this);
    this.frameCounter = this.frameCounter.bind(this);
    this.stop = this.stop.bind(this);
  }

  start() {
    this.requestId = window.requestAnimationFrame(this.frameCounter);
  }

  frameCounter(time) {
    (!this.startTime) && (this.startTime = time);
    this.frames++;
    this.requestId = window.requestAnimationFrame(this.frameCounter);
  }

  stop() {
    this.requestId && window.cancelAnimationFrame(this.requestId);
    const endTime = window.performance && window.performance.now();
    const seconds = (endTime - this.startTime) / 1000; 
    const fps = this.frames / seconds; 
    this.requestId = undefined;
    this.frames = 0;
    this.startTime = null;
    return fps;
  }
}

export {
  PerformanceFPS as default
};