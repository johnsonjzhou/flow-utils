/**
 * Miscellaneous UI helper functions
 * @author Johnson Zhou <johnson@simplepharmacy.io>
 * 
 * @export  rgba
 * @export  lighten
 * @export  darken
 * @export  blend
 * @export  msieVersion
*/
import Color from 'color';

/**
 * import and use like sass rgba()
 * @param  {string}  hex 
 * @param  {decimal}  alpha 
 * @return  {string}  rgba(x,x,x,x)
 */
const rgba = (hex, alpha) => {
  const c = Color(hex).alpha(alpha);
  let rgba = c.rgb().array();
  return `rgba(${rgba.toString()})`;
}

/**
 * import and use like sass lighten()
 * @param  {string}  hex 
 * @param  {decimal}  amount
 * @return  {string}  hex color code
 */
const lighten = (hex, amount) => {
  const c = Color(hex).lighten(amount);
  return c.hex();
}

/**
 * Blends two colors together based on a specified amount
 * @param  {string}  hex1
 * @param  {string}  hex2
 * @param  {decimal}  amount - value of 1 means entirely hex2
 * @return  {string}  hex color code
 */
const blend = (hex1, hex2, amount = 0.5) => {
  const c = Color(hex1).mix(Color(hex2), amount);
  return c.hex();
}

/**
 * import and use like sass lighten()
 * @param  {string}  hex 
 * @param  {decimal}  amount
 * @return  {string}  hex color code
 */
const darken = (hex, amount) => {
  const c = Color(hex).darken(amount);
  return c.hex();
}

/**
  * Check if user is using Internet Explorer
  * @see https://stackoverflow.com/questions/19999388/check-if-user-is-using-ie
  * @return  {int|false}  - version number if IE
  */
 const msieVersion = () => {
  const ua = window.navigator.userAgent;
  const msie = ua.indexOf("MSIE ");

  // return the version number or false 
  return (msie > 0) && parseInt(ua.substring(msie + 5, ua.indexOf(".", msie))) 
  || false;
}

/**
 * Shows a prompt before window unloads
 * eg: "there are unsaved changes on this page, do you want to reload?"
 * @param  {BeforeUnloadEvent}  event
 */
const beforeUnload = (event) => {
  event.preventDefault();
  event.returnValue = '';
}

export {
  rgba, 
  lighten, 
  blend, 
  darken,
  msieVersion, 
  beforeUnload, 
};