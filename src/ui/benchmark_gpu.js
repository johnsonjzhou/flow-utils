/**
 * Does a quick stress test for GPU based transitions and triggers an
 * event that indicates whether GPU capacity is 'normal' or 'slow'
 * @author  Johnson Zhou <johnson@simplyuseful.io>
 * 
 * @method  measure
 * @fires  window#benchmark-gpu-result
 * 
 * @param  {object}  object
 * @param  {string}  object.tag - mount point for stress testing elements
 * @param  {int}  object.duration - duration of transition in milliseconds
 * @param  {int}  object.iterations - how many passes to use in measurement
 */
import PerformanceFPS from './performance_fps';

class BenchmarkGPU {

  constructor({
    tag = 'main',
    duration = 100,
    iterations = 5,
  } = {}) {
    
    // methods
    this.measure = this.measure.bind(this);
    this.triggerTransition = this.triggerTransition.bind(this);
    this.reset = this.reset.bind(this);
    this.dispatchResult = this.dispatchResult.bind(this);

    // DOM nodes
    this.mountPoint = document.getElementsByTagName(tag)[0];
    this.styleNode = null;
    this.mountNode = null;
    this.measureNode = null;

    // measurement parameters
    this.duration = duration; // milliseconds
    this.iterations = iterations;
    this.pass = 0;
    this.performanceFPS = new PerformanceFPS();

    this.dummyText = `The quick brown fox jumped over the wall.`;
  }

  /**
   * Measures the GPU performance by stress testing transition animations:
   * 1. Build and inject elements and styles
   * 2. Trigger a transition after all elements and styles are loaded
   */
  measure() {
    // build styles
    const styles = document.createElement('style');
    const styleString = `
      #benchmark-gpu {
        position: fixed;
        height: 100vh;
        width: 100vw;
        top: 0px;
        left: 0px;
        z-index: -10000;

        transform-style: flat;
        perspective: 500px;
        perspective-origin: 0% 30%;
      }

      #benchmark-gpu .layer:nth-last-child(11) { --d: 9; }
      #benchmark-gpu .layer:nth-last-child(10) { --d: 8; }
      #benchmark-gpu .layer:nth-last-child(9) { --d: 7; }
      #benchmark-gpu .layer:nth-last-child(8) { --d: 6; }
      #benchmark-gpu .layer:nth-last-child(7) { --d: 5; }
      #benchmark-gpu .layer:nth-last-child(6) { --d: 4; }
      #benchmark-gpu .layer:nth-last-child(5) { --d: 3; }
      #benchmark-gpu .layer:nth-last-child(4) { --d: 2; }
      #benchmark-gpu .layer:nth-last-child(3) { --d: 1; }
      #benchmark-gpu .layer:nth-last-child(2) { --d: 0; }
      #benchmark-gpu .layer:nth-last-child(1) { --d: -1; }

      #benchmark-gpu .layer {
        position: absolute;
        height: 100%;
        width: 100%;
        top: 0;
        left: 0;
        background: linear-gradient(120deg, #7F165B 0%, #FF505F 100%);
        display: flex;

        --t: ${this.duration / 1000}s;
        --shift: calc(-50px * var(--d));
        --scale: calc(1 - (0.02 * var(--d)));
        --blur: calc(6px * var(--d));

        transform: 
          translate3d(var(--shift), 0, var(--shift)) 
          scale3d(var(--scale), var(--scale), 1)
        ;
        filter: blur(var(--blur));
        transition: 
          transform var(--t) ease, 
          filter var(--t) ease
        ;
        will-change: transform;
      }

      #benchmark-gpu .layer .content {
        height: auto;
        width: auto;
        transform: translateZ(0);
        background: linear-gradient(120deg, #7F165B 0%, #FF505F 100%);
      }
      
    `;
    styles.textContent = styleString;
    styles.setAttribute('data-benchmark-gpu', 'active');

    // build wrapper element
    const wrapper = document.createElement('section');
    wrapper.setAttribute('id', 'benchmark-gpu');

    // build layer elements
    const layer = document.createElement('div');
    layer.classList.add('layer');

    for (let i = 0; i < 5; i++) {
      const layer = this.createLayer();
      (i === 4) && (this.measureNode = layer);
      wrapper.appendChild(layer);
    }

    // mount
    this.mountNode = this.mountPoint.appendChild(wrapper);
    this.styleNode = document.head.appendChild(styles);

    // wait 100ms so everything is loaded to the DOM
    window.setTimeout(this.triggerTransition, 100);
  }

  /**
   * Generates div.layer with several elements on it
   */
  createLayer(childCount = 5) {
    const layer = document.createElement('div');
    layer.classList.add('layer');

    for (let x = 0; x < childCount; x++) {
      const content = document.createElement('div');
      content.classList.add('content');
      content.textContent = this.dummyText;
      layer.appendChild(content);
    }
    return layer;
  }

  /**
   * Triggers a transition event by adding a new layer element
   * Pass #0 - not measured, allows time for the GPU to 'warm up'
   * Pass #1 to this.iterations - measure FPS, whilst looping trigger
   */
  triggerTransition() { 
    if (
      this.mountNode === null || 
      this.measureNode === null || 
      this.styleNode === null
    ) return;

    // start the measurement on pass #1
    if (this.pass === 1) {
      const measureDuration = this.duration * this.iterations;
      this.performanceFPS.start();
      window.setTimeout(() => {
        const fps = this.performanceFPS.stop();
        this.reset();
        this.dispatchResult(fps);
      }, measureDuration);
    }

    // add a new element to trigger a transition
    const layer = this.createLayer();
    this.mountNode.appendChild(layer);

    // loop triggers based on defined iterations
    if (this.pass < this.iterations) {
      (this.pass === 0) && 
      window.setTimeout(this.triggerTransition, (this.duration * 2));

      (this.pass > 0) && 
      window.setTimeout(this.triggerTransition, this.duration);

      this.pass++;
    }
  }

  /**
   * @param  {float}  fps
   * @fires  window#benchmark-gpu-result
   */
  dispatchResult(fps = 0) {
    window.Console && window.Console.log(`FPS ${fps}`, 'GPU');
    window.dispatchEvent(new CustomEvent(
      `benchmark-gpu-result`, { detail: fps }
    ));
  }

  /**
   * Removes all DOM nodes and resets measurement metrics
   */
  reset() {
    this.measureNode.remove();
    this.mountNode.remove();
    this.styleNode.remove();
    this.mountNode = null;
    this.measureNode = null;
    this.styleNode = null;
    this.pass = 0;
  }
}

export {
  BenchmarkGPU as default
};