/**
 * Styled ErrorBoundary
 * @author Johnson Zhou <johnson@simplyuseful.io>
 * 
 * @export  ErrorBoundary (default)
 * @export  ErrorMessage (ErrorMessageMemo)
 * 
*/
import React, { PureComponent, memo } from 'react';
import { error } from './styles/error.jsx';

/**
 * @param  {string}  props.message
 */
const ErrorMessage = (props) => {
  const { message, children } = props;
  return (
    <error.wrapper >
      <error.message>{message || children || 'An error occurred'}</error.message>
    </error.wrapper>
  );
};

const ErrorMessageMemo = memo(ErrorMessage);

/**
 * To catch an error in children and render an ErrorMessage
 * @param  {string}  props.message - specify an error message
 */
class ErrorBoundary extends PureComponent {

	constructor(props) {
    super(props);

    this.state = {
      hasError: false,
    };

    // error info
    this.info = null;
  }

  componentDidUpdate() {
    const { 
      children 
    } = this.props; 

    const {
      hasError 
    } = this.state;

    // Reset hasError state if children is null 
    hasError && (children === null) && this.setState({ hasError: false });
  }

	componentDidCatch(error, info) {

    // not sure we need that much detail
    // if (info.componentStack) this.info = info.componentStack;
    
    // app notification system
    (window.Console && window.Console.handleError(error));

    // Display fallback UI
    this.setState({ hasError: true });
	}

	render() {
    const { 
      message, 
      children,
    } = this.props;

    const { 
      hasError 
    } = this.state;

    return(
      ((hasError) && 
      <ErrorMessageMemo>{message}</ErrorMessageMemo>) || 
      children
    );
	}
}

export {
  ErrorBoundary as default,
  ErrorMessageMemo as ErrorMessage
};