# Flow Utilities

A collection of miscellaneous utilities and helper functions.  

---
## Install
````bash
npm install https://bitbucket.org/johnsonjzhou/flow-utils.git
````

---
## Version  
1.0.5 [changelog](./doc/changelog.md).  

---
## Contents 
[Console](./doc/console.md)  
[Web Workers](./doc/web_worker.md)  
[Misc UI Helpers](./doc/misc_ui_helpers.md)  
[Misc Data Helpers](./doc/misc_data_helpers.md)  
[Color Scheme](./doc/color_scheme.md)  
[BenchmarkGPU](./doc/benchmark_gpu.md)  
[Perspective3D](./doc/perspective_3d.md)  

---
## License
MIT.  
Johnson Zhou [johnson@simplyuseful.io](mailto://johnson@simplyuseful.io).  